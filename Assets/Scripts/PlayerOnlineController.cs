using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOnlineController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject _bluePrefab;

    [SerializeField]
    private GameObject _redPrefab;


    private List<GameObject> _allChesses = new();

    private bool isMasterPlayerTurn;
    private ExitGames.Client.Photon.Hashtable playerProperties = new();

    // Start is called before the first frame update
    void Start()
    {
        var touchController = FindObjectOfType<TouchInputController>();
        touchController.OnInputClick += OnInputClick;
        isMasterPlayerTurn = true;
        playerProperties["giveUp"] = false;
    }

    private void OnInputClick(Vector3 clickPosition, bool isDoubleClick, bool isLongTap)
    {
        if (isMasterPlayerTurn && !PhotonNetwork.IsMasterClient)
            return;
        else if (PhotonNetwork.IsMasterClient && !isMasterPlayerTurn)
            return;

        if (GameManager.Instance.CurrentState == GameState.Waiting) return;

        var worldPosition = Camera.main.ScreenToWorldPoint(clickPosition);
        var gridCellPosition = Map.Instance.GetCellGridPosition(worldPosition);

        var result = GameManager.Instance.SetOccupiedPoint(gridCellPosition, true);

        if (result)
        {
            playerProperties["addPositionX"] = gridCellPosition.x;
            playerProperties["addPositionY"] = gridCellPosition.y;
            playerProperties["isMasterPlayerTurn"] = isMasterPlayerTurn;
            isMasterPlayerTurn = !isMasterPlayerTurn;
            var worldCellPosition = Map.Instance.GetCellWorldPosition(gridCellPosition);
            _allChesses.Add(Instantiate(_redPrefab, worldCellPosition, Quaternion.identity));
            PhotonNetwork.SetPlayerCustomProperties(playerProperties);
        }
        else
        {
            Debug.Log("Failed");
        }
    }

    public void OnAIInputClick(int posX, int posY)
    {
        if (GameManager.Instance.CurrentState == GameState.Waiting) return;

        var gridCellPosition = new Vector2Int(posX, posY);
        var worldCellPosition = Map.Instance.GetCellWorldPosition(gridCellPosition);
        _allChesses.Add(Instantiate(_bluePrefab, worldCellPosition, Quaternion.identity));

        isMasterPlayerTurn = true;
    }

    public void ClearMap()
    {
        foreach (var item in _allChesses)
        {
            Destroy(item);
        }
        _allChesses.Clear();
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        if (!targetPlayer.IsLocal)
        {
            var opponentGiveUp = bool.Parse(changedProps["giveUp"].ToString());
            if (opponentGiveUp)
            {
                GameManager.Instance.OnWinOnline();
                return;
            }

            var isMasterPlayerTurnProperty = bool.Parse(changedProps["isMasterPlayerTurn"].ToString());
            var positionX = int.Parse(changedProps["addPositionX"].ToString());
            var positionY = int.Parse(changedProps["addPositionY"].ToString());

            var point = new Vector2Int(positionX, positionY);

            GameManager.Instance.SetOccupiedForOpponent(point);
            var worldCellPosition = Map.Instance.GetCellWorldPosition(point);
            _allChesses.Add(Instantiate(_bluePrefab, worldCellPosition, Quaternion.identity));
            this.isMasterPlayerTurn = !isMasterPlayerTurnProperty;
        }
        else
        {
            var opponentGiveUp = bool.Parse(changedProps["giveUp"].ToString());
            if (opponentGiveUp)
            {
                GameManager.Instance.OnLoseOnline();
                return;
            }
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
    }

    public void GiveUpOnline()
    {
        playerProperties["giveUp"] = true;
        PhotonNetwork.SetPlayerCustomProperties(playerProperties);
    }
}
