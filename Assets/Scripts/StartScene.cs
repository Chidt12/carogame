using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum OfflineModeType
{
    Easy = 0,
    Hard = 1,
}

public class StartScene : MonoBehaviour
{
    [SerializeField]
    private Button onlineModeButton;
    [SerializeField]
    private Button offlineModeButton;
    [SerializeField]
    private Button homeButton;
    [SerializeField]
    private Button easyMode;
    [SerializeField]
    private Button hardMode;

    [SerializeField]
    private GameObject startSceneGameObject;
    [SerializeField]
    private GameObject offlineSceneGameObject;
    [SerializeField]
    private GameObject onlineSceneGameObject;


    private void Awake()
    {
        onlineModeButton.onClick.AddListener(OnEnterOnlineMode);
        offlineModeButton.onClick.AddListener(OnEnterOfflineMode);
        homeButton.onClick.AddListener(OnBackHome);
        easyMode.onClick.AddListener(OnSelectEasyMode);
        hardMode.onClick.AddListener(OnSelectHardMode);

        OnBackHome();
    }

    private void OnEnterOnlineMode()
    {
        SceneManager.LoadScene("ConnectToServer");
    }

    private void OnEnterOfflineMode()
    {
        startSceneGameObject.SetActive(false);
        offlineSceneGameObject.SetActive(true);
        onlineSceneGameObject.SetActive(false);
    }

    private void OnBackHome()
    {
        startSceneGameObject.SetActive(true);
        offlineSceneGameObject.SetActive(false);
        onlineSceneGameObject.SetActive(false);
    }

    private void OnSelectEasyMode()
    {
        GeneralSettings.Instance.Depth = 1;
        OnSelectMode(OfflineModeType.Easy);
    }

    private void OnSelectHardMode()
    {
        GeneralSettings.Instance.Depth = 2;
        OnSelectMode(OfflineModeType.Hard);
    }

    private void OnSelectMode(OfflineModeType mode)
    {
        SceneManager.LoadScene("GameplayScreen");
    }

}
