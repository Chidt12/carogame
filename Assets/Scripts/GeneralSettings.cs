using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralSettings : MonoSingleton<GeneralSettings>
{

    public int Depth { get; set; }

    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(gameObject);
    }
}
