using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ConnectToServer : MonoBehaviourPunCallbacks
{
    public TMP_InputField usernameInput;
    public Button connectButton;

    public Button _backButton;
    public GameObject _loadingGameObject;
    public GameObject _connectGameObject;

    private void Awake()
    {
        connectButton.onClick.AddListener(OnClickConnect);
        _backButton.onClick.AddListener(OnBack);
    }

    private void Start()
    {
        _loadingGameObject.gameObject.SetActive(false);
        _connectGameObject.gameObject.SetActive(true);
    }

    private void OnBack()
    {

        SceneManager.LoadScene("StartScene");
    }

    public void OnClickConnect()
    {
        if(usernameInput.text.Length >= 1)
        {
            PhotonNetwork.NickName = usernameInput.text;
            PhotonNetwork.AutomaticallySyncScene = true;
            PhotonNetwork.ConnectUsingSettings();
            _loadingGameObject.gameObject.SetActive(true);
            _connectGameObject.gameObject.SetActive(false);
        }
    }

    public override void OnConnectedToMaster()
    {
        SceneManager.LoadScene("LobbyScene");
    }


}
