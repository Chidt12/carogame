﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum StoneSetBlockType
{ 
    BothSideFree = 0,
    SingleSideBlocked = 1,
    BothSideBlocked = 2
}

public enum PlayerType
{
    AI  = 0, // AI
    Player = 1 // Player
}

public class MinimaxResult
{
    public double? score;
    public int? movePosX;
    public int? movePosY;

    public MinimaxResult(double? score = null, int? movePosX = null, int? movePosY = null)
    {
        this.score = score;
        this.movePosX = movePosX;
        this.movePosY = movePosY;
    }
}

public class Minimax
{
    public const int NUMBER_CONSECUTIVE_STONE_WIN_GAME = 5;
    public const int WIN_SCORE = 1_000_000_000;
    private Board currentBoard;

    public Minimax(Board board)
    {
        currentBoard = board;
    }

    public static int GetScore(Board board, bool forPlayer, bool playerTurn)
    {
        PointType[,] boardMatrix = board.BoardMatrix;

        var horizontalScore = EvaluateHorizontal(boardMatrix, forPlayer, playerTurn);
        var diagonalScore = EvaluateDiagonal(boardMatrix, forPlayer, playerTurn);
        var verticalScore = EvaluateVertical(boardMatrix, forPlayer, playerTurn);

        return horizontalScore + diagonalScore + verticalScore;
    }

    public static double EvaluateBoardForAI(Board board, bool playerTurn)
    {
        double playerScore = GetScore(board, true, playerTurn);
        double AIScore = GetScore(board, false, playerTurn);

        if (playerScore == 0) playerScore = 1.0;

        // Calculate relative score
        return AIScore / playerScore;
    }

    public int[] CalculateNextMove(int depth)
    {
        int[] move = new int[2];

        var bestMove = SearchWinningMove(currentBoard);

        if(bestMove != null)
        {
            move[0] = (int)bestMove.movePosX;
            move[1] = (int)bestMove.movePosY;
        }   
        else
        {
            bestMove = MinimaxSearchAB(depth, currentBoard, true, double.MinValue, WIN_SCORE);
            if (bestMove.movePosX == null)
                move = null;
            else
            {
                move[0] = (int)bestMove.movePosX;
                move[1] = (int)bestMove.movePosY;
            }
        }

        return move;
    }

    /*
	 * alpha : Best AI Move (Max)
	 * beta : Best Player Move (Min)
	 * returns: {score, move[0], move[1]}
	 * */
    public static MinimaxResult MinimaxSearchAB(int depth, Board board, bool maxTurn, double alpha, double beta)
    {
        // Last depth (or termial node), evaluate the current board score.
        if(depth == 0)
        {
            var x = new MinimaxResult(EvaluateBoardForAI(board, !maxTurn));
            return x;
        }

        List<int[]> allPossibleMoves = board.GenerateMoves();

        // If there is no possible move left, treat this node as a terminal node and return the score.
        if (allPossibleMoves.Count == 0)
        {
            var x = new MinimaxResult(EvaluateBoardForAI(board, !maxTurn));
            return x;
        }

        MinimaxResult bestMove = new MinimaxResult();

        if (maxTurn)
        {
            // Initialize the starting best move with score -infinity.
            bestMove.score = double.MinValue;

            foreach (var move in allPossibleMoves)
            {
                Board dummyBoard = new Board(board);

                dummyBoard.AddStone(move[0], move[1], maxTurn);

                MinimaxResult tempMove = MinimaxSearchAB(depth - 1, dummyBoard, !maxTurn, alpha, beta);


                // Update alpha
                if(tempMove.score != null && tempMove.score > alpha)
                {
                    alpha = (double)tempMove.score;
                }

                // Pruning beta
                if(tempMove.score >= beta)
                {
                    return tempMove;
                }

                // Find the move with better score;
                if((double)tempMove.score > (double)bestMove.score)
                {
                    bestMove = tempMove;
                    bestMove.movePosX = move[0];
                    bestMove.movePosY = move[1];
                }
            }
        }
        else
        {
            // Initialize the starting best move using the first move in the list and +infinity score.
            bestMove.score = double.MaxValue;
            bestMove.movePosX = allPossibleMoves[0][0];
            bestMove.movePosY = allPossibleMoves[0][1];

            foreach (var move in allPossibleMoves)
            {
                Board dummyBoard = new Board(board);

                dummyBoard.AddStone(move[0], move[1], maxTurn);

                MinimaxResult tempMove = MinimaxSearchAB(depth - 1, dummyBoard, !maxTurn, alpha, beta);


                // Update beta
                if (tempMove.score != null && tempMove.score < alpha)
                {
                    beta = (double)tempMove.score;
                }

                // Pruning alpha
                if (tempMove.score <= alpha)
                {
                    return tempMove;
                }

                // Find the move with better score;
                if ((double)tempMove.score < (double)bestMove.score)
                {
                    bestMove = tempMove;
                    bestMove.movePosX = move[0];
                    bestMove.movePosY = move[1];
                }
            }
        }

        Debug.Log($"depth {depth} score {bestMove.score} point {bestMove.movePosX}-{bestMove.movePosY} max: {maxTurn} alpha: {alpha} beta: {beta}");
        return bestMove;
    }

    private static MinimaxResult SearchWinningMove(Board board) // Score, move[0], move[1]
    {
        List<int[]> allPossibleMoves = board.GenerateMoves();
        MinimaxResult winningMove = new MinimaxResult();

        foreach (var move in allPossibleMoves)
        {
            Board dummyBoard = new Board(board);

            dummyBoard.AddStone(move[0], move[1], true);

            if(GetScore(dummyBoard, false, false) >= WIN_SCORE)
            {
                winningMove.movePosX = move[0];
                winningMove.movePosY = move[1];
                return winningMove;
            }
        }

        return null;
    }

    public static int EvaluateVertical(PointType[,] boardMatrix, bool forPlayer, bool playerTurn)
    {
        int consecutive = 0;
        int blocks = (int)StoneSetBlockType.BothSideBlocked;
        int score = 0;

        for (int i = 0; i < Board.WIDTH; i++)
        {
            // Reset consecutive stone and blocks count
            consecutive = 0;
            blocks = (int)StoneSetBlockType.BothSideBlocked;

            for (int j = 0; j < Board.HEIGHT; j++)
            {
                if (boardMatrix[i, j] == (forPlayer ? PointType.Player : PointType.AI))
                    consecutive++;
                else if (boardMatrix[i, j] == PointType.Empty)
                {
                    if (consecutive > 0)
                    {
                        blocks--;
                        score += GetConsecutiveSetScore(consecutive, (StoneSetBlockType)blocks, forPlayer == playerTurn);

                        // Reset Consecutive stone count.
                        consecutive = 0;
                    }

                    // Cell hiện tại là khoảng trống nên consecutive tiếp theo bị chặn nhiều nhất là 1 bên.
                    blocks = (int)StoneSetBlockType.SingleSideBlocked;
                }
                // Cell được chiếm bởi opponent.
                else
                {
                    if (consecutive > 0)
                    {
                        score += GetConsecutiveSetScore(consecutive, (StoneSetBlockType)blocks, forPlayer == playerTurn);

                        // Reset Consecutive stone count.
                        consecutive = 0;
                    }

                    // Current cell is occupied by opponent, consecutive tiếp theo có thể bị chặn bởi 2 blocks;
                    blocks = (int)StoneSetBlockType.BothSideBlocked;
                }
            }

            // End of row, check if there were any consecutive stones before we reached right border
            if (consecutive > 0)
            {
                score += GetConsecutiveSetScore(consecutive, (StoneSetBlockType)blocks, forPlayer == playerTurn);
            }
        }

        return score;
    }

    public static int EvaluateHorizontal(PointType[,] boardMatrix, bool forPlayer, bool playerTurn)
    {
        int consecutive = 0;
        int blocks = (int)StoneSetBlockType.BothSideBlocked;
        int score = 0;

        for (int j = 0; j < Board.HEIGHT; j++)
        {
            // Reset consecutive stone and blocks count
            consecutive = 0;
            blocks = (int)StoneSetBlockType.BothSideBlocked;

            for (int i = 0; i < Board.WIDTH; i++)
            {
                if (boardMatrix[i, j] == (forPlayer ? PointType.Player : PointType.AI))
                    consecutive++;
                else if (boardMatrix[i,j] == PointType.Empty)
                {
                    if(consecutive > 0)
                    {
                        blocks--;
                        score += GetConsecutiveSetScore(consecutive, (StoneSetBlockType)blocks, forPlayer == playerTurn);

                        // Reset Consecutive stone count.
                        consecutive = 0;
                    }

                    // Cell hiện tại là khoảng trống nên consecutive tiếp theo bị chặn nhiều nhất là 1 bên.
                    blocks = (int)StoneSetBlockType.SingleSideBlocked;
                }
                // Cell được chiếm bởi opponent.
                else
                {
                    if (consecutive > 0)
                    {
                        score += GetConsecutiveSetScore(consecutive, (StoneSetBlockType)blocks, forPlayer == playerTurn);

                        // Reset Consecutive stone count.
                        consecutive = 0;
                    }

                    // Current cell is occupied by opponent, consecutive tiếp theo có thể bị chặn bởi 2 blocks;
                    blocks = (int)StoneSetBlockType.BothSideBlocked;
                }
            }

            // End of row, check if there were any consecutive stones before we reached right border
            if (consecutive > 0)
            {
                score += GetConsecutiveSetScore(consecutive, (StoneSetBlockType)blocks, forPlayer == playerTurn);
            }
        }

        return score;
    }

    public static int EvaluateDiagonal(PointType[,] boardMatrix, bool forPlayer, bool playerTurn)
    {
        int consecutive = 0;
        int blocks = (int)StoneSetBlockType.BothSideBlocked;
        int score = 0;

        // From top - left to bottom - right diagonally
        for (int k = 0; k < (Board.WIDTH - 1) + (Board.HEIGHT - 1); k++)
        {
            int iStart = Math.Max(0, k - (Board.HEIGHT - 1));
            int iEnd = Math.Min(Board.WIDTH - 1, k);

            consecutive = 0;
            blocks = (int)StoneSetBlockType.BothSideBlocked;

            for (int i = iStart; i < iEnd; i++)
            {
                int j = k - i;

                if (boardMatrix[i, j] == (forPlayer ? PointType.Player : PointType.AI))
                    consecutive++;
                else if (boardMatrix[i, j] == PointType.Empty)
                {
                    if (consecutive > 0)
                    {
                        blocks--;
                        score += GetConsecutiveSetScore(consecutive, (StoneSetBlockType)blocks, forPlayer == playerTurn);

                        // Reset Consecutive stone count.
                        consecutive = 0;
                    }

                    // Cell hiện tại là khoảng trống nên consecutive tiếp theo bị chặn nhiều nhất là 1 bên.
                    blocks = (int)StoneSetBlockType.SingleSideBlocked;
                }
                // Cell được chiếm bởi opponent.
                else
                {
                    if (consecutive > 0)
                    {
                        score += GetConsecutiveSetScore(consecutive, (StoneSetBlockType)blocks, forPlayer == playerTurn);

                        // Reset Consecutive stone count.
                        consecutive = 0;
                    }

                    // Current cell is occupied by opponent, consecutive tiếp theo có thể bị chặn bởi 2 blocks;
                    blocks = (int)StoneSetBlockType.BothSideBlocked;
                }
            }

            // End of row, check if there were any consecutive stones before we reached right border
            if (consecutive > 0)
            {
                score += GetConsecutiveSetScore(consecutive, (StoneSetBlockType)blocks, forPlayer == playerTurn);
            }
        }

        // From bottom - left to top - right diagonally
        for (int k = 1 - Board.HEIGHT; k < Board.WIDTH; k++)
        {
            consecutive = 0;
            blocks = 2;

            int iStart = Math.Max(0, k);
            int iEnd = Math.Min(Board.HEIGHT - 1 + k, Board.WIDTH - 1);

            for (int i = iStart; i < iEnd; i++)
            {
                int j = i - k;

                if (boardMatrix[i, j] == (forPlayer ? PointType.Player : PointType.AI))
                    consecutive++;
                else if (boardMatrix[i, j] == PointType.Empty)
                {
                    if (consecutive > 0)
                    {
                        blocks--;
                        score += GetConsecutiveSetScore(consecutive, (StoneSetBlockType)blocks, forPlayer == playerTurn);

                        // Reset Consecutive stone count.
                        consecutive = 0;
                    }

                    // Cell hiện tại là khoảng trống nên consecutive tiếp theo bị chặn nhiều nhất là 1 bên.
                    blocks = (int)StoneSetBlockType.SingleSideBlocked;
                }
                // Cell được chiếm bởi opponent.
                else
                {
                    if (consecutive > 0)
                    {
                        score += GetConsecutiveSetScore(consecutive, (StoneSetBlockType)blocks, forPlayer == playerTurn);

                        // Reset Consecutive stone count.
                        consecutive = 0;
                    }

                    // Current cell is occupied by opponent, consecutive tiếp theo có thể bị chặn bởi 2 blocks;
                    blocks = (int)StoneSetBlockType.BothSideBlocked;
                }
            }

            // End of row, check if there were any consecutive stones before we reached right border
            if (consecutive > 0)
            {
                score += GetConsecutiveSetScore(consecutive, (StoneSetBlockType)blocks, forPlayer == playerTurn);
            }
        }

        return score;
    }

    public static int GetConsecutiveSetScore(int count, StoneSetBlockType blockType, bool currentTurn)
    {
        int winGuarantee = 1_000_000; // Give big number to sure that will make bot get decision immediately;

        if (blockType == StoneSetBlockType.BothSideBlocked && count < 5) 
            return 0;

        switch (count)
        {
            case 5:
                return WIN_SCORE;
            case 4:
                // Sure win, just put the 5th after the set;
                if (currentTurn)
                    return winGuarantee;
                else
                {
                    if (blockType == StoneSetBlockType.BothSideFree)
                        return winGuarantee / 4;
                    else
                        // If only a single side is blocked, 4 consecutive stones limits the opponents move
                        // (Opponent can only replace a stone that will block the remaining side, otherwise the game is lost in the next turn). So a relatively high score is given for this set.
                        return 200;
                }
            case 3:
                if (blockType == StoneSetBlockType.BothSideFree)
                {
                    if (currentTurn)
                        return 50_000;
                    else
                        return 200;
                }
                else
                {
                    if (currentTurn)
                        return 10;
                    else
                        return 5;
                }
            case 2:
                if (blockType == StoneSetBlockType.BothSideFree)
                    if (currentTurn)
                        return 7;
                    else
                        return 5;
                else
                    return 3;
            case 1:
                return 1;

            default:
                break;
        }

        return WIN_SCORE * 2;
    }
}