using System.Collections.Generic;
using System.Linq;

public enum PointType
{
    Empty = 0,
    AI = 1,
    Player = 2
}

public class Board
{

    public const int WIDTH = 16;
    public const int HEIGHT = 10;

    private PointType[,] boardMatrix;
    public PointType[,] BoardMatrix => boardMatrix;

    public Board()
    {
        boardMatrix = new PointType[WIDTH, HEIGHT];
    }

    public Board(Board board)
    {
        PointType[,] matrixCopy = board.BoardMatrix;
        boardMatrix = new PointType[WIDTH, HEIGHT];

        for (int i = 0; i < WIDTH; i++)
        {
            for (int j = 0; j < HEIGHT; j++)
            {
                boardMatrix[i, j] = matrixCopy[i, j];
            }
        }

    }

    public bool IsValid(int posX, int posY)
    {
        if (posX >= WIDTH || posX < 0 || posY >= HEIGHT || posY < 0)
            return false;
        return true;
    }

    public bool AddStone(int posX, int posY, bool isAI)
    {
        if (boardMatrix[posX, posY] != PointType.Empty) return false;
        boardMatrix[posX, posY] = isAI ? PointType.AI : PointType.Player;
        return true;
    }

    // Predict that if let the bot put the chess near other stone is better than have no stone around.
    public List<int[]> GenerateMoves()
    {
        List<int[]> moveList = new();

        for (int i = 0; i < WIDTH; i++)
        {
            for (int j = 0; j < HEIGHT; j++)
            {
                if (boardMatrix[i, j] != PointType.Empty)
                    continue;

                // Look for cells that has at least one stone in an adjacent cell.
                if (i > 0)
                {
                    if (j > 0)
                    {
                        if (boardMatrix[i - 1, j - 1] != PointType.Empty ||
                           boardMatrix[i, j - 1] != PointType.Empty)
                        {
                            int[] move = { i, j };
                            moveList.Add(move);
                            continue;
                        }
                    }
                    if (j < HEIGHT - 1)
                    {
                        if (boardMatrix[i - 1,j + 1] != PointType.Empty ||
                           boardMatrix[i,j + 1] != PointType.Empty)
                        {
                            int[] move = { i, j };
                            moveList.Add(move);
                            continue;
                        }
                    }
                    if(boardMatrix[i-1,j] != PointType.Empty)
                    {
                        int[] move = { i, j };
                        moveList.Add(move);
                        continue;
                    }
                }

                if(i < WIDTH - 1)
                {
                    if (j > 0)
                    {
                        if (boardMatrix[i + 1,j - 1] != PointType.Empty ||
                           boardMatrix[i, j - 1] != PointType.Empty)
                        {
                            int[] move = { i, j };
                            moveList.Add(move);
                            continue;
                        }
                    }
                    if (j < HEIGHT - 1)
                    {
                        if (boardMatrix[i + 1, j + 1] != PointType.Empty ||
                           boardMatrix[i, j + 1] != PointType.Empty)
                        {
                            int[] move = { i, j };
                            moveList.Add(move);
                            continue;
                        }
                    }
                    if (boardMatrix[i + 1, j] != PointType.Empty)
                    {
                        int[] move = { i, j };
                        moveList.Add(move);
                        continue;
                    }
                }
            }
        }

        return moveList;
    }
}