using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoSingleton<PlayerController>
{
    [SerializeField]
    private GameObject _bluePrefab;

    [SerializeField]
    private GameObject _redPrefab;


    private List<GameObject> _allChesses = new();

    private bool _isPlayerTurn;

    // Start is called before the first frame update
    void Start()
    {
        var touchController = FindObjectOfType<TouchInputController>();
        touchController.OnInputClick += OnInputClick;
        _isPlayerTurn = true;
    }

    private void OnInputClick(Vector3 clickPosition, bool isDoubleClick, bool isLongTap)
    {
        if (!_isPlayerTurn)
            return;

        if (GameManager.Instance.CurrentState == GameState.Waiting) return;

        var worldPosition = Camera.main.ScreenToWorldPoint(clickPosition);
        var gridCellPosition = Map.Instance.GetCellGridPosition(worldPosition);


        var result = GameManager.Instance.SetOccupiedPoint(gridCellPosition);

        if (result)
        {
            var worldCellPosition = Map.Instance.GetCellWorldPosition(gridCellPosition);
            _allChesses.Add(Instantiate(_redPrefab, worldCellPosition, Quaternion.identity));

            _isPlayerTurn = false;
        }
        else
        {
            Debug.Log("Failed");
        }
    }

    public void OnAIInputClick(int posX, int posY)
    {
        if (GameManager.Instance.CurrentState == GameState.Waiting) return;

        var gridCellPosition = new Vector2Int(posX, posY);
        var worldCellPosition = Map.Instance.GetCellWorldPosition(gridCellPosition);
        _allChesses.Add(Instantiate(_bluePrefab, worldCellPosition, Quaternion.identity));

        _isPlayerTurn = true;
    }

    public void CLearMap()
    {
        foreach (var item in _allChesses)
        {
            Destroy(item);
        }
        _allChesses.Clear();
    }
}
