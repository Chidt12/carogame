using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using System.Linq;

public class LobbyManager : MonoBehaviourPunCallbacks
{
    public TMP_InputField roomInputField;
    public GameObject lobbyPanel;
    public GameObject roomPanel;
    public TextMeshProUGUI roomName;
    public TextMeshProUGUI otherPlayerText;
    public Button readyButton;

    public RoomItem roomItemPrefab;
    private List<RoomItem> _roomItemList = new List<RoomItem>();
    public Transform contentObject;

    private float _nextUpdateLobby;
    private float _delayTime = 1;
    Player _player;

    private void Start()
    {
        lobbyPanel.SetActive(true);
        roomPanel.SetActive(false);
        _player = PhotonNetwork.LocalPlayer;
        PhotonNetwork.JoinLobby();  
    }

    public void OnClickCreate()
    {
        if(roomInputField.text.Length >= 1)
        {
            PhotonNetwork.CreateRoom(roomInputField.text, new RoomOptions() { MaxPlayers = 2, BroadcastPropsChangeToAll = true });
        }
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedLobby();
        lobbyPanel.SetActive(false);
        roomPanel.SetActive(true);

        roomName.text = "Room Name: " + PhotonNetwork.CurrentRoom.Name;
        if(PhotonNetwork.CurrentRoom.PlayerCount >= 2)
        {
            var otherPlayer = PhotonNetwork.CurrentRoom.Players.FirstOrDefault(x => x.Value.UserId != _player.UserId);
            otherPlayerText.text = $"{otherPlayer.Value.NickName} Is Entered The Room!";
        }
        else
        {
            otherPlayerText.text = "Only you in this room. Waiting for other!";
        }
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);
        if(_nextUpdateLobby < Time.time)
        {
            _nextUpdateLobby = Time.time + _delayTime;
            UpdateRoomList(roomList);
        }
    }

    private void UpdateRoomList(List<RoomInfo> roomInfos)
    {
        foreach (var item in _roomItemList)
        {
            Destroy(item.gameObject);
        }

        _roomItemList.Clear();

        foreach (var item in roomInfos)
        {
            RoomItem newRoom = Instantiate(roomItemPrefab, contentObject);
            newRoom.SetRoomName(item.Name);
            _roomItemList.Add(newRoom);
        }
    }

    public void JoinRoom(string roomName)
    {
        PhotonNetwork.JoinRoom(roomName);
    }

    public void OnClickLeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public void OnClickLeaveLobby()
    {
        PhotonNetwork.LeaveLobby();
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene("StartScene");
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        roomPanel.SetActive(false);
        lobbyPanel.SetActive(true);
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("Joined Lobby");
        base.OnJoinedLobby();
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        PhotonNetwork.JoinLobby();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        otherPlayerText.text = $"{newPlayer.NickName} Is Entered The Room!";
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        otherPlayerText.text = "Only you in this room. Waiting for other!";
    }

    private void Update()
    {
        if(PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount >= 2)
        {
            readyButton.gameObject.SetActive(true);
        }
        else
        {
            readyButton.gameObject.SetActive(false);
        }
    }

    public void OnClickPlayerGame()
    {
        PhotonNetwork.CurrentRoom.IsOpen = false;
        PhotonNetwork.CurrentRoom.IsVisible = false;
        PhotonNetwork.LoadLevel("GameplayOnline");
    }
}
