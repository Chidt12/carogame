﻿using Cysharp.Threading.Tasks;
using Photon.Pun;
using System;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum GameState
{
    Playing,
    Waiting
}

public class GameManager : MonoSingleton<GameManager>
{
    [SerializeField]
    private uint _checkNumber;
    [SerializeField]
    private GameObject winPanel;
    [SerializeField]
    private GameObject darkPanel;
    [SerializeField]
    private GameObject losePanel;
    [SerializeField]
    private Button winReplayButton;
    [SerializeField]
    private Button winHomeButton;
    [SerializeField]
    private Button loseReplayButton;
    [SerializeField]
    private Button loseHomeButton;
    [SerializeField]
    private GameObject winOnlinePanel;
    [SerializeField]
    private GameObject loseOnlinePanel;

    private Board _currentBoard;
    private Minimax _minimax;
    private GameState _currentState;
    private CancellationTokenSource _cancellationTokenSource;

    public GameState CurrentState => _currentState;

    protected override void Awake()
    {
        base.Awake();
        loseReplayButton.onClick.AddListener(Replay);
        winReplayButton.onClick.AddListener(Replay);
        winHomeButton.onClick.AddListener(GoHome);
        loseHomeButton.onClick.AddListener(GoHome);

        _currentBoard = new Board();
        _minimax = new Minimax(_currentBoard);

        StartGame();
    }

    private void Replay()
    {
        PlayerController.Instance.CLearMap();
        _currentBoard = new Board();
        _minimax = new Minimax(_currentBoard);
        darkPanel.SetActive(false);
        winPanel.SetActive(false);
        losePanel.SetActive(false);
        StartTurn().Forget();
    }

    private async UniTask StartTurn()
    {
        await UniTask.Delay(TimeSpan.FromSeconds(0.3f));
        _currentState = GameState.Playing;
    }

    private void StartGame()
    {
        _currentState = GameState.Playing;
        darkPanel.SetActive(false);
        winPanel.SetActive(false);
        losePanel.SetActive(false);
        loseOnlinePanel?.SetActive(false);
        winOnlinePanel?.SetActive(false);
    }

    private void GoHome()
    {
        SceneManager.LoadScene("StartScene");
    }

    public void SetOccupiedForOpponent(Vector2Int point)
    {
        if (!_currentBoard.IsValid(point.x, point.y)) return;
        var result = _currentBoard.AddStone(point.x, point.y, true);
        if (result)
        {
            CheckWinnerForOnline();
        }
    }

    public bool SetOccupiedPoint(Vector2Int point, bool isOnlineMode = false)
    {
        if (!_currentBoard.IsValid(point.x, point.y)) return false;
        var result = _currentBoard.AddStone(point.x, point.y, false);

        if (result)
        {
            if (isOnlineMode)
            {
                CheckWinnerForOnline();
            }
            else
            {
                var notFinished = CheckWinner();

                if (notFinished)
                {
                    _cancellationTokenSource?.Cancel();
                    _cancellationTokenSource = new CancellationTokenSource();
                    StartAITurnAsync(_cancellationTokenSource.Token).Forget();
                }
            }

        }

        return result;
    }

    private async UniTask StartAITurnAsync(CancellationToken token)
    {
        await UniTask.Delay(TimeSpan.FromSeconds(1), cancellationToken: token);
        var move = _minimax.CalculateNextMove(GeneralSettings.Instance.Depth);
        if (move != null)
        {
            var result = _currentBoard.AddStone(move[0], move[1], true);

            if (result)
            {
                PlayerController.Instance.OnAIInputClick(move[0], move[1]);
                var notFinished = CheckWinner();
            }
        }
    }

    private bool CheckWinner()
    {
        if (Minimax.GetScore(_currentBoard, true, false) >= Minimax.WIN_SCORE)
        {
            _currentState = GameState.Waiting;
            darkPanel.SetActive(true);
            losePanel.SetActive(false);
            winPanel.SetActive(true);
            return false;
        }
        if (Minimax.GetScore(_currentBoard, false, true) >= Minimax.WIN_SCORE)
        {
            _currentState = GameState.Waiting;
            darkPanel.SetActive(true);
            losePanel.SetActive(true);
            winPanel.SetActive(false);
            return false;
        }

        return true;
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene("LobbyScene");
    }

    protected bool CheckWinnerForOnline()
    {
        if (Minimax.GetScore(_currentBoard, true, false) >= Minimax.WIN_SCORE)
        {
            OnWinOnline();
            return false;
        }
        if (Minimax.GetScore(_currentBoard, false, true) >= Minimax.WIN_SCORE)
        {
            OnLoseOnline();
            return false;
        }
        return true;
    }

    public void GiveUp()
    {
        _cancellationTokenSource?.Cancel();
        _currentState = GameState.Waiting;
        darkPanel.SetActive(true);
        losePanel.SetActive(true);
        winPanel.SetActive(false);
    }

    public void OnLoseOnline()
    {
        _currentState = GameState.Waiting;
        darkPanel.SetActive(true);
        loseOnlinePanel?.SetActive(true);
    }

    public void OnWinOnline()
    {
        _currentState = GameState.Waiting;
        darkPanel.SetActive(true);
        winOnlinePanel?.SetActive(true);
    }
}
